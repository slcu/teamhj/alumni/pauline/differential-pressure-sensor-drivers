﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InterfaceKit_full.Acquisition
{
    public partial class AcquisitionBase : UserControl
    {
        protected double _timerAnchorInSeconds;

        public AcquisitionBase()
        {
            InitializeComponent();
        }

        //public AllData

        public virtual void ChangeValue(int val)
        {
        }

        public virtual void ResetAcquisition()
        {
        }

        public virtual List<List<float>> GetAllData()
        {
            return new List<List<float>>();
        }

        public string FormatDataToString()
        {
            var alldata = GetAllData();

            //Get expected number of rows
            var numberOfRows = alldata[0].Count;

            var rows =
                Enumerable.Range(0, numberOfRows) //For each row
                .Select(row => alldata.Select(list => list[row].ToString()).ToArray()) //Get row data from all columns
                .ToList();

            StringBuilder sb = new StringBuilder();

            foreach (var row in rows)
            {
                sb.AppendLine(string.Join(",", row));
            }

            return sb.ToString();
        }

        protected void startTimer()
        {
            _timerAnchorInSeconds = getTimeNowInSeconds();
        }

        protected double getTimeNowInSeconds()
        {
            return DateTime.Now.Subtract(DateTime.MinValue).TotalSeconds;
        }
    }
}
