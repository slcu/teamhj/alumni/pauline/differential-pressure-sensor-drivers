import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import os

filename = "C:\\Users\\pkb\\Documents\\test.csv"

df = pd.read_csv(filename)

axis_time = df["0"]
data_kpa = df["0.1"]
data_psi = df["0.2"]

fig, ax = plt.subplots()
ax.plot(axis_time, data_kpa, label="kPa")
ax.plot(axis_time, data_psi, label="psi")

plt.show()
